```plantuml
!include test.puml
```

```plantuml
@startuml pump_changeover

participant main

box "Entities" 
    participant Pump1 as p1
    participant Pump2 as p2
    participant "<<PumpProtection>>\nMaxRunTime1" as mr1
    participant "<<PumpProtection>>\nMaxRunTime2" as mr2
    participant "<<ConfigurableSetting>>\nMaximumRunTimeSetting1" as mrs1
    participant "<<ConfigurableSetting>>\nMaximumRunTimeSetting2" as mrs2
end box

box "Usecases" #lightblue
    participant "Pump\nDispatcher" as pd
    participant "Change\nOver" as co
end box

main -> p1 : doWork
activate p1
p1 -> mr1 : mustChangeOver()
activate mr1
p1 <- mr1 : False
deactivate mr1
...
note over p1
    Iterate over
    all protections
end note
...
main <- p1
deactivate p1

main -> p2 : doWork
activate p2
p2 -> mr2 : mustChangeOver()
activate mr2
p2 <- mr2 : True
deactivate mr2
p2 -\ pd : notify_cb
note left of p2
   Callback registered
   during construction
end note
activate pd
pd -> co : queueChange(Pump2 to Pump1)
p2 <- pd
deactivate pd
main <- p2
deactivate p2

main -> pd : doWork
activate pd
pd -> co : doWork
activate co 
pd -> p1 : start
activate p1
p1 -> p1 : clearNonRunningTime
pd <-- p1
deactivate p1

pd -> p2 : stop
activate p2
p2 -> p2 : clearRunningTime
pd <-- p2
deactivate p2

co -> co : removePumpFromChangeOverQueue




@enduml
```